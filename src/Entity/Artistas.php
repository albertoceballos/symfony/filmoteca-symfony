<?php

namespace App\Entity;

use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Doctrine\ORM\Mapping as ORM;

/**
 * Artistas
 *
 * @ORM\Table(name="artistas")
 * @ORM\Entity(repositoryClass="App\Repository\ArtistasRepository")
 */
class Artistas
{
    /**
     * @var int
     *
     * @ORM\Column(name="id_artista", type="integer", nullable=false)
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="IDENTITY")
     */
    private $idArtista;

    /**
     * @var string|null
     *
     * @ORM\Column(name="artista", type="string", length=255, nullable=true)
     */
    private $artista;

    /**
     * @var int|null
     *
     * @ORM\Column(name="nacimiento", type="integer", nullable=true)
     */
    private $nacimiento;
    
     /**
     * @var \Doctrine\Common\Collections\Collection
     *
     * @ORM\ManyToMany(targetEntity="Producciones", mappedBy="idArtista")
     */
    private $idProduccion;

    /**
     * Constructor
     */
    public function __construct()
    {
        $this->idProduccion = new \Doctrine\Common\Collections\ArrayCollection();
    }
    

    public function getIdArtista()
    {
        return $this->idArtista;
    }

    public function getArtista()
    {
        return $this->artista;
    }

    public function setArtista(string $artista): self
    {
        $this->artista = $artista;

        return $this;
    }

    public function getNacimiento()
    {
        return $this->nacimiento;
    }

    public function setNacimiento(int $nacimiento): self
    {
        $this->nacimiento = $nacimiento;

        return $this;
    }
    
     public function getIdProduccion(): Collection
    {
        return $this->idProduccion;
    }

     public function addIdProduccion(Producciones $idProduccion): self
     {
         if (!$this->idProduccion->contains($idProduccion)) {
             $this->idProduccion[] = $idProduccion;
             $idProduccion->addIdGenero($this);
         }

         return $this;
     }

     public function removeIdProduccion(Producciones $idProduccion): self
     {
         if ($this->idProduccion->contains($idProduccion)) {
             $this->idProduccion->removeElement($idProduccion);
             $idProduccion->removeIdGenero($this);
         }

         return $this;
     }


}
