<?php

namespace App\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * Temporadas
 *
 * @ORM\Table(name="temporadas", indexes={@ORM\Index(name="id_produccion", columns={"id_produccion"})})
 * @ORM\Entity
 */
class Temporadas
{
    /**
     * @var int
     *
     * @ORM\Column(name="id_temporada", type="integer", nullable=false)
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="IDENTITY")
     */
    private $idTemporada;

    /**
     * @var int
     *
     * @ORM\Column(name="capitulos", type="integer", nullable=false)
     */
    private $capitulos;

    /**
     * @var float|null
     *
     * @ORM\Column(name="vistos", type="float", precision=10, scale=0, nullable=true)
     */
    private $vistos;

    /**
     * @var int
     *
     * @ORM\Column(name="temporada", type="integer", nullable=false)
     */
    private $temporada;

    /**
     * @var \Producciones
     *
     * @ORM\ManyToOne(targetEntity="Producciones")
     * @ORM\JoinColumns({
     *   @ORM\JoinColumn(name="id_produccion", referencedColumnName="id_produccion")
     * })
     */
    private $idProduccion;

    public function getIdTemporada(): ?int
    {
        return $this->idTemporada;
    }

    public function getCapitulos(): ?int
    {
        return $this->capitulos;
    }

    public function setCapitulos(int $capitulos): self
    {
        $this->capitulos = $capitulos;

        return $this;
    }

    public function getVistos(): ?float
    {
        return $this->vistos;
    }

    public function setVistos(?float $vistos): self
    {
        $this->vistos = $vistos;

        return $this;
    }

    public function getTemporada(): ?int
    {
        return $this->temporada;
    }

    public function setTemporada(int $temporada): self
    {
        $this->temporada = $temporada;

        return $this;
    }

    public function getIdProduccion(): ?Producciones
    {
        return $this->idProduccion;
    }

    public function setIdProduccion(?Producciones $idProduccion): self
    {
        $this->idProduccion = $idProduccion;

        return $this;
    }


}
