<?php

namespace App\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * Participan
 *
 * @ORM\Table(name="participan", indexes={@ORM\Index(name="id_rol", columns={"id_rol"}), @ORM\Index(name="id_artista", columns={"id_artista"}), @ORM\Index(name="IDX_9DD38FB3BBA95E11", columns={"id_produccion"})})
 * @ORM\Entity
 */
class Participan
{
    /**
     * @var \Producciones
     *
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="NONE")
     * @ORM\OneToOne(targetEntity="Producciones")
     * @ORM\JoinColumns({
     *   @ORM\JoinColumn(name="id_produccion", referencedColumnName="id_produccion")
     * })
     */
    private $idProduccion;

    /**
     * @var \Artistas
     *
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="NONE")
     * @ORM\OneToOne(targetEntity="Artistas")
     * @ORM\JoinColumns({
     *   @ORM\JoinColumn(name="id_artista", referencedColumnName="id_artista")
     * })
     */
    private $idArtista;

    /**
     * @var \Roles
     *
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="NONE")
     * @ORM\OneToOne(targetEntity="Roles")
     * @ORM\JoinColumns({
     *   @ORM\JoinColumn(name="id_rol", referencedColumnName="id_rol")
     * })
     */
    private $idRol;

    public function getIdProduccion()
    {
        return $this->idProduccion;
    }

    public function setIdProduccion(Producciones $idProduccion): self
    {
        $this->idProduccion = $idProduccion;

        return $this;
    }

    public function getIdArtista()
    {
        return $this->idArtista;
    }

    public function setIdArtista(Artistas $idArtista): self
    {
        $this->idArtista = $idArtista;

        return $this;
    }

    public function getIdRol()
    {
        return $this->idRol;
    }

    public function setIdRol(Roles $idRol): self
    {
        $this->idRol = $idRol;

        return $this;
    }


}
