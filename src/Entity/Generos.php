<?php

namespace App\Entity;

use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Doctrine\ORM\Mapping as ORM;

/**
 * Generos
 *
 * @ORM\Table(name="generos", uniqueConstraints={@ORM\UniqueConstraint(name="genero", columns={"genero"})})
 * @ORM\Entity
 */
class Generos
{
    /**
     * @var int
     *
     * @ORM\Column(name="id_genero", type="integer", nullable=false)
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="IDENTITY")
     */
    private $idGenero;

    /**
     * @var string
     *
     * @ORM\Column(name="genero", type="string", length=255, nullable=false)
     */
    private $genero;

    /**
     * @var \Doctrine\Common\Collections\Collection
     *
     * @ORM\ManyToMany(targetEntity="Producciones", mappedBy="idGenero")
     */
    private $idProduccion;

    /**
     * Constructor
     */
    public function __construct()
    {
        $this->idProduccion = new \Doctrine\Common\Collections\ArrayCollection();
    }

    public function getIdGenero()
    {
        return $this->idGenero;
    }

    public function getGenero()
    {
        return $this->genero;
    }

    public function setGenero(string $genero): self
    {
        $this->genero = $genero;

        return $this;
    }

    /**
     * @return Collection|Producciones[]
     */
    public function getIdProduccion(): Collection
    {
        return $this->idProduccion;
    }

    public function addIdProduccion(Producciones $idProduccion): self
    {
        if (!$this->idProduccion->contains($idProduccion)) {
            $this->idProduccion[] = $idProduccion;
            $idProduccion->addIdGenero($this);
        }

        return $this;
    }

    public function removeIdProduccion(Producciones $idProduccion): self
    {
        if ($this->idProduccion->contains($idProduccion)) {
            $this->idProduccion->removeElement($idProduccion);
            $idProduccion->removeIdGenero($this);
        }

        return $this;
    }

}
