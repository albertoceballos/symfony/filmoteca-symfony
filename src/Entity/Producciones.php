<?php

namespace App\Entity;

use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Doctrine\ORM\Mapping as ORM;

use Symfony\Component\Validator\Constraints as Assert;

/**
 * Producciones
 *
 * @ORM\Table(name="producciones")
 * @ORM\Entity(repositoryClass="App\Repository\ProduccionesRepository")
 */
class Producciones
{
    /**
     * @var int
     *
     * @ORM\Column(name="id_produccion", type="integer", nullable=false)
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="IDENTITY")
     */
    private $idProduccion;

    /**
     * @var string
     *
     * @ORM\Column(name="titulo", type="string", length=255, nullable=false)
     */
    private $titulo;

    /**
     * @var int|null
     *
     * @ORM\Column(name="estreno", type="integer", nullable=true)
     * @Assert\NotBlank
     */
    private $estreno;

    /**
     * @var float|null
     *
     * @ORM\Column(name="puntuacion", type="float", precision=10, scale=0, nullable=true)
     * @Assert\NotBlank
     * @Assert\Regex("/[0-9]*\.?[0-9]/", message="Formato no válido")
     */
    private $puntuacion;

    /**
     * @var bool|null
     *
     * @ORM\Column(name="vos", type="boolean", nullable=true)
     */
    private $vos;

    /**
     * @var string|null
     *
     * @ORM\Column(name="nacionalidad", type="string", length=255, nullable=true)
     */
    private $nacionalidad;

    /**
     * @var bool|null
     *
     * @ORM\Column(name="vista", type="boolean", nullable=true)
     * 
     */
    private $vista;

    /**
     * @var string|null
     *
     * @ORM\Column(name="cartel", type="string", length=255, nullable=true)
     */
    private $cartel;

    /**
     * @var \Doctrine\Common\Collections\Collection
     *
     * @ORM\ManyToMany(targetEntity="Franquicias", inversedBy="idProduccion")
     * @ORM\JoinTable(name="pertenece",
     *   joinColumns={
     *     @ORM\JoinColumn(name="id_produccion", referencedColumnName="id_produccion")
     *   },
     *   inverseJoinColumns={
     *     @ORM\JoinColumn(name="id_franquicia", referencedColumnName="id_franquicia")
     *   }
     * )
     */
    private $idFranquicia;

    /**
     * @var \Doctrine\Common\Collections\Collection
     *
     * @ORM\ManyToMany(targetEntity="Generos", inversedBy="idProduccion")
     * @ORM\JoinTable(name="proyecta",
     *   joinColumns={
     *     @ORM\JoinColumn(name="id_produccion", referencedColumnName="id_produccion")
     *   },
     *   inverseJoinColumns={
     *     @ORM\JoinColumn(name="id_genero", referencedColumnName="id_genero")
     *   }
     * )
     */
    private $idGenero;
    
     /**
     * @var \Doctrine\Common\Collections\Collection
     *
     * @ORM\ManyToMany(targetEntity="Artistas", inversedBy="idProduccion")
     * @ORM\JoinTable(name="participan",
     *   joinColumns={
     *     @ORM\JoinColumn(name="id_produccion", referencedColumnName="id_produccion")
     *   },
     *   inverseJoinColumns={
     *     @ORM\JoinColumn(name="id_artista", referencedColumnName="id_artista")
     *   }
     * )
     */
    private $idArtista;
    

    /**
     * Constructor
     */
    public function __construct()
    {
        $this->idFranquicia = new \Doctrine\Common\Collections\ArrayCollection();
        $this->idGenero = new \Doctrine\Common\Collections\ArrayCollection();
        $this->idArtista = new ArrayCollection();
    }

    public function getIdProduccion()
    {
        return $this->idProduccion;
    }

    public function getTitulo()
    {
        return $this->titulo;
    }

    public function setTitulo(string $titulo): self
    {
        $this->titulo = $titulo;

        return $this;
    }

    public function getEstreno()
    {
        return $this->estreno;
    }

    public function setEstreno(int $estreno)
    {
        $this->estreno = $estreno;

        return $this;
    }

    public function getPuntuacion()
    {
        return $this->puntuacion;
    }

    public function setPuntuacion(float $puntuacion)
    {
        $this->puntuacion = $puntuacion;

        return $this;
    }

    public function getVos()
    {
        return $this->vos;
    }

    public function setVos(bool $vos)
    {
        $this->vos = $vos;

        return $this;
    }

    public function getNacionalidad()
    {
        return $this->nacionalidad;
    }

    public function setNacionalidad(string $nacionalidad)
    {
        $this->nacionalidad = $nacionalidad;

        return $this;
    }

    public function getVista()
    {
        return $this->vista;
    }

    public function setVista(bool $vista)
    {
        $this->vista = $vista;

        return $this;
    }

    public function getCartel()
    {
        return $this->cartel;
    }

    public function setCartel(string $cartel)
    {
        $this->cartel = $cartel;

        return $this;
    }

    /**
     * @return Collection|Franquicias[]
     */
    public function getIdFranquicia(): Collection
    {
        return $this->idFranquicia;
    }

    public function addIdFranquicium(Franquicias $idFranquicium): self
    {
        if (!$this->idFranquicia->contains($idFranquicium)) {
            $this->idFranquicia[] = $idFranquicium;
        }

        return $this;
    }

    public function removeIdFranquicium(Franquicias $idFranquicium): self
    {
        if ($this->idFranquicia->contains($idFranquicium)) {
            $this->idFranquicia->removeElement($idFranquicium);
        }

        return $this;
    }

    /**
     * @return Collection|Generos[]
     */
    public function getIdGenero(): Collection
    {
        return $this->idGenero;
    }

    public function addIdGenero(Generos $idGenero): self
    {
        if (!$this->idGenero->contains($idGenero)) {
            $this->idGenero[] = $idGenero;
        }

        return $this;
    }

    public function removeIdGenero(Generos $idGenero): self
    {
        if ($this->idGenero->contains($idGenero)) {
            $this->idGenero->removeElement($idGenero);
        }

        return $this;
    }

    /**
     * @return Collection|Artistas[]
     */
    public function getIdArtista(): Collection
    {
        return $this->idArtista;
    }

    public function addIdArtistum(Artistas $idArtistum): self
    {
        if (!$this->idArtista->contains($idArtistum)) {
            $this->idArtista[] = $idArtistum;
        }

        return $this;
    }

    public function removeIdArtistum(Artistas $idArtistum): self
    {
        if ($this->idArtista->contains($idArtistum)) {
            $this->idArtista->removeElement($idArtistum);
        }

        return $this;
    }
    

}
