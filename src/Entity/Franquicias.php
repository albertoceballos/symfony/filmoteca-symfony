<?php

namespace App\Entity;

use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Doctrine\ORM\Mapping as ORM;

/**
 * Franquicias
 *
 * @ORM\Table(name="franquicias", uniqueConstraints={@ORM\UniqueConstraint(name="franquicia", columns={"franquicia"})})
 * @ORM\Entity
 */
class Franquicias
{
    /**
     * @var int
     *
     * @ORM\Column(name="id_franquicia", type="integer", nullable=false)
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="IDENTITY")
     */
    private $idFranquicia;

    /**
     * @var string|null
     *
     * @ORM\Column(name="franquicia", type="string", length=255, nullable=true)
     */
    private $franquicia;

    /**
     * @var \Doctrine\Common\Collections\Collection
     *
     * @ORM\ManyToMany(targetEntity="Producciones", mappedBy="idFranquicia")
     */
    private $idProduccion;

    /**
     * Constructor
     */
    public function __construct()
    {
        $this->idProduccion = new \Doctrine\Common\Collections\ArrayCollection();
    }

    public function getIdFranquicia()
    {
        return $this->idFranquicia;
    }

    public function getFranquicia()
    {
        return $this->franquicia;
    }

    public function setFranquicia(string $franquicia): self
    {
        $this->franquicia = $franquicia;

        return $this;
    }

    /**
     * @return Collection|Producciones[]
     */
    public function getIdProduccion(): Collection
    {
        return $this->idProduccion;
    }

    public function addIdProduccion(Producciones $idProduccion): self
    {
        if (!$this->idProduccion->contains($idProduccion)) {
            $this->idProduccion[] = $idProduccion;
            $idProduccion->addIdFranquicium($this);
        }

        return $this;
    }

    public function removeIdProduccion(Producciones $idProduccion): self
    {
        if ($this->idProduccion->contains($idProduccion)) {
            $this->idProduccion->removeElement($idProduccion);
            $idProduccion->removeIdFranquicium($this);
        }

        return $this;
    }

}
