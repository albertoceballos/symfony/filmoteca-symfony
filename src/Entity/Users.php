<?php

namespace App\Entity;

use Doctrine\ORM\Mapping as ORM;

//clase necesario para usar la utentificacion de usuarios
use Symfony\Component\Security\Core\User\UserInterface;

//cargo clase para la validación de los formularios y pogo un alias
use Symfony\Component\Validator\Constraints as Assert;

/**
 * @ORM\Entity(repositoryClass="App\Repository\UsersRepository")
 */
class Users implements UserInterface
{
    /**
     * @ORM\Id()
     * @ORM\GeneratedValue()
     * @ORM\Column(type="integer")
     */
    private $id;

    /**
     * @ORM\Column(type="string", length=50)
     */
    private $role;

    /**
     * @ORM\Column(type="string", length=255)
     * @Assert\NotBlank
     * @Assert\Regex("/[a-zA-Z ]+/", message="El nombre debe esta compuesto por letras o espacios")
     */
    private $name;

    /**
     * @ORM\Column(type="string", length=255, nullable=true)
     * @Assert\Regex("/[a-zA-Z ]+/", message="El apellido debe esta compuesto por letras o espacios")
     */
    private $surname;

    /**
     * @ORM\Column(type="string", length=255)
     * @Assert\Regex("/[a-zA-Z ]+/")
     * @Assert\Email(message="El E-mail: '{{ value }}' no es válido")
     */
    private $email;

    /**
     * @ORM\Column(type="string", length=255)
     * @Assert\NotBlank
     */
    private $password;

    public function getId()
    {
        return $this->id;
    }

    public function getRole()
    {
        return $this->role;
    }

    public function setRole(string $role): self
    {
        $this->role = $role;

        return $this;
    }

    public function getName()
    {
        return $this->name;
    }

    public function setName(string $name): self
    {
        $this->name = $name;

        return $this;
    }

    public function getSurname()
    {
        return $this->surname;
    }

    public function setSurname(string $surname): self
    {
        $this->surname = $surname;

        return $this;
    }

    public function getEmail()
    {
        return $this->email;
    }

    public function setEmail(string $email): self
    {
        $this->email = $email;

        return $this;
    }

    public function getPassword()
    {
        return $this->password;
    }

    public function setPassword(string $password): self
    {
        $this->password = $password;

        return $this;
    }
    
    //funciones que han de ser implemetadas al usar el UserInterface:
    public function getUsername() {
        return $this->email;
    }
    
    public function getRoles() {
        return ['ROLE_USER'];
    }
    
    public function getSalt() {
        return null;
    }
    
    public function eraseCredentials() {
        
    }
}
