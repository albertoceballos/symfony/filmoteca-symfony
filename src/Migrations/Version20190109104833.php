<?php declare(strict_types=1);

namespace DoctrineMigrations;

use Doctrine\DBAL\Schema\Schema;
use Doctrine\Migrations\AbstractMigration;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
final class Version20190109104833 extends AbstractMigration
{
    public function up(Schema $schema) : void
    {
        // this up() migration is auto-generated, please modify it to your needs
        $this->abortIf($this->connection->getDatabasePlatform()->getName() !== 'mysql', 'Migration can only be executed safely on \'mysql\'.');

        $this->addSql('CREATE TABLE users (id INT AUTO_INCREMENT NOT NULL, role VARCHAR(50) NOT NULL, name VARCHAR(255) NOT NULL, surname VARCHAR(255) DEFAULT NULL, email VARCHAR(255) NOT NULL, password VARCHAR(255) NOT NULL, PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci ENGINE = InnoDB');
        $this->addSql('ALTER TABLE pertenece RENAME INDEX id_franquicia TO IDX_D2DA2B04F01D1D53');
        $this->addSql('ALTER TABLE proyecta RENAME INDEX id_genero TO IDX_886A2FBE86373DD7');
        $this->addSql('ALTER TABLE temporadas CHANGE id_temporada id_temporada INT AUTO_INCREMENT NOT NULL');
    }

    public function down(Schema $schema) : void
    {
        // this down() migration is auto-generated, please modify it to your needs
        $this->abortIf($this->connection->getDatabasePlatform()->getName() !== 'mysql', 'Migration can only be executed safely on \'mysql\'.');

        $this->addSql('DROP TABLE users');
        $this->addSql('ALTER TABLE pertenece RENAME INDEX idx_d2da2b04f01d1d53 TO id_franquicia');
        $this->addSql('ALTER TABLE proyecta RENAME INDEX idx_886a2fbe86373dd7 TO id_genero');
        $this->addSql('ALTER TABLE temporadas CHANGE id_temporada id_temporada INT NOT NULL');
    }
}
