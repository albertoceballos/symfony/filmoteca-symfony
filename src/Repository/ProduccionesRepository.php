<?php

namespace App\Repository;

use App\Entity\Producciones;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Symfony\Bridge\Doctrine\RegistryInterface;


class ProduccionesRepository extends ServiceEntityRepository {

   
    public function __construct(RegistryInterface $registry) {
        parent::__construct($registry, Producciones::class);       
   
    }
    
    public function BarraBusqueda($busqueda): array{
        //Consulta mediante QueryBuilder para la barra de Búsqueda
        $qb= $this->createQueryBuilder('p')
                ->where('p.titulo LIKE :busqueda')
                ->orWhere('p.estreno LIKE :busqueda')
                ->orWhere('p.nacionalidad LIKE :busqueda')
                ->setParameter('busqueda', '%'.$busqueda.'%')
                ->orderBy('p.titulo','ASC')
                ->getQuery();
        
        return $qb->execute();
                
    }
    
    public function ProduccionesPorArtista($id){
        //consulta mediante DQL para buscar por artista:
        $em=$this->getEntityManager();
        
        $dql=$em->createQuery('SELECT p FROM App\Entity\Producciones p'
                . ' JOIN App\Entity\Participan p1'
                . ' WITH p.idProduccion=p1.idProduccion'
                . ' WHERE p1.idArtista=:id_artista')
                ->setParameter('id_artista',$id);
                
        return $dql->execute();
        
    }
}

