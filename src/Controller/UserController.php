<?php

namespace App\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\Routing\Annotation\Route;

//para usar el codificador:
use Symfony\Component\Security\Core\Encoder\UserPasswordEncoderInterface;

//para la autenticación de usuarios
use Symfony\Component\Security\Http\Authentication\AuthenticationUtils;

//cargo la clase de mi formulario:
use App\Form\RegisterType;

//cargamos la entidad:
use App\Entity\Users;



use Symfony\Component\HttpFoundation\Request;

class UserController extends AbstractController
{
    /**
     * @Route("/registro", name="register")
     */
    public function register(Request $request, UserPasswordEncoderInterface $encoder)
    {
        $creado=false;
        //crear un objeto de clase Usuario
        $user=new Users();
        
        //crear el formulario con la clase que hemos creado y pasarle el obejeto $user
        $form=$this->createForm(RegisterType::class,$user);
        
        //relleno el formulario
        $form->handleRequest($request);
        
        //comprobar sise envía el formulario y modificar el obejeto $user para guardarlo
        if($form->isSubmitted() && $form->isValid()){
            $user->setRole('ROLE_USER');
            
            //cifrando la contraseña:
            $encoded_password=$encoder->encodePassword($user, $user->getPassword());
            $user->setPassword($encoded_password);
            
            //Guardo el objeto:
            $em= $this->getDoctrine()->getManager();
            $em->persist($user);
            $em->flush();
            
            $creado=true;
        }
        
        
        
        
        
        $generos_repo= $this->getDoctrine()->getRepository(\App\Entity\Generos::class);
        $generos=$generos_repo->findAll();
        
        return $this->render('user/register.html.twig', [
            'form'=>$form->createView(),
            'generos'=>$generos,
            'creado'=>$creado,
        ]);
    }
    
     /**
     * @Route("/login", name="login")
     */
    public function login(AuthenticationUtils $auth){
        $error=$auth->getLastAuthenticationError();
        $lastusername=$auth->getLastUsername();
        
        
        $generos_repo= $this->getDoctrine()->getRepository(\App\Entity\Generos::class);
        $generos=$generos_repo->findAll();
        return $this->render('user/login.html.twig',[
            'error'=>$error,
            'last_username'=>$lastusername,
            'generos'=>$generos,
        ]);
    }
}
