<?php

namespace App\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\Routing\Annotation\Route;

//la clase Request necesaria para cargar los datos de formularios:
use Symfony\Component\HttpFoundation\Request;

//cargo el formulario que he creado para crear producciones: 
use App\Form\ProduccionType;

//cargo la entidad
use App\Entity\Producciones;

use App\Entity\Generos;

class ProduccionesController extends AbstractController {

    /**
     * @Route("/detalle/{id}", name="detail")
     */
    public function detail($id) {
        
        //Extraigo los datos de la producción con un find
        $prod_repo= $this->getDoctrine()->getRepository(Producciones::class);
        $produccion=$prod_repo->find($id);
        
        
        //necesito pasarle también todos los géneros porque si no me da error con la cabecera:
        $generos_repo= $this->getDoctrine()->getRepository(\App\Entity\Generos::class);
        $generos=$generos_repo->findAll();
        return $this->render('producciones/detail.html.twig',[
            'produccion'=>$produccion,
            'generos'=>$generos,
            ]);
    }
    
    /**
     * @Route("/crear", name="create")
     */
    public function create(Request $request){
        
        
        $produccion=new Producciones();
        /*para que saque un género ya por defecto para insertar creo un obejeto géneros
         y al objeto produccion le añado una nueva colección del nuevo objeto género: */
        $genero=new Generos();
        $produccion->getIdGenero()->add($genero);
        
        //creo el nuevo formulario y le paso el objeto $produccion:
        $form=$this->createForm(ProduccionType::class,$produccion);
        $form->handleRequest($request);
        
        if($form->isSubmitted() && $form->isValid()){
            
            $entityManager= $this->getDoctrine()->getManager();
            $entityManager->persist($produccion);
            $entityManager->flush();
            
            
            return $this->redirect($this->generateUrl('detail',['id'=>$produccion->getIdProduccion()]));
        }
        
        
        $generos_repo= $this->getDoctrine()->getRepository(\App\Entity\Generos::class);
        $generos=$generos_repo->findAll();
        
        return $this->render('producciones/create.html.twig',[
            'generos'=>$generos,
            'form'=>$form->createView(),
           
        ]);
    }
    
    /**
     * @Route("/editar/{id}", name="edit")
     */
    public function edit(Request $request, Producciones $produccion){
        $form=$this->createForm(ProduccionType::class,$produccion);
        $form->handleRequest($request);
        
        if($form->isSubmitted() && $form->isValid()){
            
            $entityManager= $this->getDoctrine()->getManager();
            $entityManager->persist($produccion);
            $entityManager->flush();
            
            
            return $this->redirect($this->generateUrl('detail',['id'=>$produccion->getIdProduccion()]));
        }
        
        
        $generos_repo= $this->getDoctrine()->getRepository(\App\Entity\Generos::class);
        $generos=$generos_repo->findAll();
        
        return $this->render('producciones/create.html.twig',[
            'generos'=>$generos,
            'form'=>$form->createView(),
            'edit'=>true,
        ]);
        
    }
    
     /**
     * @Route("/borrar/{id}", name="delete")
     */
    public function delete(Producciones $produccion){
        
        $entityManager= $this->getDoctrine()->getManager();
        $entityManager->remove($produccion);
        $entityManager->flush();
        
        return $this->redirectToRoute('home');
    }
    
      /**
     * @Route("/pruebas", name="pruebas")
     */
    public function pruebas(){
        
       $generos_repo= $this->getDoctrine()->getRepository(\App\Entity\Generos::class);
       $generos=$generos_repo->findAll(); 
       $form=$this->createForm(\App\Form\GenerosType::class);
       
       return $this->render('generos/pruebas.html.twig',[
           'form'=>$form->createView(),
           'generos'=>$generos,
       ]);
       
    }

}
