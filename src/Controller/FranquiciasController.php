<?php

namespace App\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\Routing\Annotation\Route;

//Entidades
use App\Entity\Generos;
use App\Entity\Franquicias;


class FranquiciasController extends AbstractController
{
    /**
     * @Route("/franquicias", name="franquicias_index")
     */
    public function index()
    {
        
        $franquicias_repo= $this->getDoctrine()->getRepository(Franquicias::class);
        $franquicias=$franquicias_repo->findAll();
        
        
        $generos_repo= $this->getDoctrine()->getRepository(Generos::class);
        $generos=$generos_repo->findAll();
        
        return $this->render('franquicias/index.html.twig', [
            'controller_name' => 'FranquiciasController',
            'generos'=>$generos,
            'franquicias'=>$franquicias,
        ]);
    }
}
