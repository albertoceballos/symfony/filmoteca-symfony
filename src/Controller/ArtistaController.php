<?php

namespace App\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Component\HttpFoundation\Request;

use App\Entity\Generos;
use App\Entity\Producciones;

class ArtistaController extends AbstractController
{
    /**
     * @Route("/artistas", name="artistas_index")
     */
    public function index(Request $request)
    {
        
        
        $generos_repo=$this->getDoctrine()->getRepository(Generos::class); 
        $generos=$generos_repo->findAll();
        
        $producciones_repo=$this->getDoctrine()->getRepository(Producciones::class);
 
        /*creo el formulario que tengo ya formateado en una clase y le paso el handleRequest
        para poder coger sus datos:*/
        $form=$this->createForm(\App\Form\ArtistasType::class); 
        $form->handleRequest($request);
        
        if($form->isSubmitted()){
            
            //cojo el dato del id del artista que mando en el request del formulario y lo meto en una variable
            $artista=$request->request->get('artistas');
            $id_artista=$artista['artistas'];      
            $producciones=$producciones_repo->ProduccionesPorArtista($id_artista);
      
            return $this->render('artista/index.html.twig',[
                'generos'=>$generos,
                'producciones'=>$producciones,
                'form'=>$form->createView(),
            ]);
          
        }
        
         
        return $this->render('artista/index.html.twig', [
            'generos'=>$generos,
            'form'=>$form->createView(),
        ]);
    }
    
}
