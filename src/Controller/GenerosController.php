<?php

namespace App\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\Routing\Annotation\Route;
use App\Entity\Generos;

class GenerosController extends AbstractController {

    /**
     * @Route("/generos/{id}", name="generos")
     */
    public function index($id) {

        $generos_repo= $this->getDoctrine()->getRepository(Generos::class);
        $generos=$generos_repo->findAll();
        
        
        /*Busco los películas de un determinado género con una consulta SQL clásica
         * Para paseaselos después en un array a la vista
        
        $em = $this->getDoctrine()->getManager();
        $conn = $em->getConnection();

       /* $sql = "SELECT * FROM producciones p
        LEFT JOIN proyecta
        USING (id_produccion)
        LEFT JOIN pertenece p1
        USING (id_produccion)
        LEFT JOIN franquicias f
        USING (id_franquicia)
        WHERE proyecta.id_genero=:id";

        $stmt=$conn->prepare($sql);
        $stmt->execute(['id'=>$id]);

        
        $producciones=$stmt->fetchAll();*/
        
        $producciones_repo= $this->getDoctrine()->getRepository(\App\Entity\Producciones::class);
        $producciones=$producciones_repo->findAll();
        



        /* Aqui podría mostrar los datos con echo y php llamando a los métodos, pero
          en vez de eso le paso el objeto $producciones a la vista y en ella recorro el objeto que
          ya me trae los generos y las franquicias enlazadas
         */

        /* foreach($producciones as $produccion){

          echo $produccion->getTitulo()."<br>";

          foreach ($produccion->getIdGenero() as $genero){

          echo $genero->getGenero()."<br>";
          }
          echo "<hr>";
          } */


        return $this->render('generos/index.html.twig', [
            'producciones'=>$producciones,
            'generos'=>$generos,
            'id'=>$id,
             ]);
    }

}
