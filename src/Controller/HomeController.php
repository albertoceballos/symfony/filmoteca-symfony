<?php

namespace App\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\Routing\Annotation\Route;
//cargar las entidades
use App\Entity\Producciones;
use App\Entity\Generos;

//clase del formulario de busqueda:
use App\Form\BusquedaType;
//clase del formulario para Ordenar:
use App\Form\OrdenarType;


use Symfony\Component\HttpFoundation\Request;

class HomeController extends AbstractController {

    /**
     * @Route("/", name="home")
     */
    public function index(Request $request) {


        $producciones_repo = $this->getDoctrine()->getRepository(Producciones::class);
        $generos_repo = $this->getDoctrine()->getRepository(Generos::class);

        $producciones = $producciones_repo->findAll();
        $generos = $generos_repo->findAll();

        $conn = $this->getDoctrine()->getConnection();

        /*$actores = [];
        foreach ($producciones as $prod) {

            //saco los actores mediante una consulta SQL que meto en un array con el id de la película

            $id = $prod->getIdProduccion();
            $sql = "SELECT a.* FROM artistas a 
          INNER JOIN participan p
          USING(id_artista)
          WHERE p.id_produccion=$id";

            $stmt = $conn->prepare($sql);
            $stmt->execute();
            $actores[$id] = $stmt->fetchAll();
        }*/

        //Para el formulario de busqueda:
            $form_busqueda=$this->createForm(BusquedaType::class);
            $form_busqueda->handleRequest($request);
            
            $busqueda="";
            
              
            if($form_busqueda->isSubmitted()){
                
                $busqueda=$request->request->get('busqueda');
                $producciones=$producciones_repo->BarraBusqueda($busqueda['busqueda']);
                
            }
            
            //Para cargar el formulario para ordenar:
            $form_ordena= $this->createForm(OrdenarType::class);
            $form_ordena->handleRequest($request);
            
            if($form_ordena->isSubmitted()){
                
                $campos_orden=$request->request->get('ordenar');
                $orden=$campos_orden['ordena'];
                $direccion=$campos_orden['direccion'];
                /*Para extraer todos los campos hago un findBy con el criterio de busqueda vacío y en el
                 * segundo argumento de orden le paso el campo por el que ordeno y la direccion(ASC o DESC); 
                */
                $producciones=$producciones_repo->findBy([],[$orden=>$direccion]);
            }


        return $this->render('home/index.html.twig', [
                    'producciones' => $producciones,
                    'generos' => $generos,
                    'form_busqueda'=>$form_busqueda->createView(),
                    'form_ordena'=>$form_ordena->createView(),
                    
        ]);
    }

}
