<?php

namespace App\Form;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Bridge\Doctrine\Form\Type\EntityType;
use Symfony\Component\Form\Extension\Core\Type\SubmitType;
use App\Entity\Artistas;

class ArtistasType extends AbstractType {

    public function buildForm(FormBuilderInterface $builder, array $options) {

        $builder->add('artistas', EntityType::class, [
            // looks for choices from this entity
            'class' => Artistas::class,
            // uses the User.username property as the visible option string
            'choice_label' => 'artista',
            'label'=>"Elige un artista:",
          
                // used to render a select box, check boxes or radios
                 'multiple' => false,
                 'expanded' => false,
            
            ]);
          
    }

}
