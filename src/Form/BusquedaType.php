<?php

namespace App\Form;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\Extension\Core\Type\SearchType;
use Symfony\Component\Form\Extension\Core\Type\SubmitType;

class BusquedaType extends AbstractType{
    
    public function buildForm(FormBuilderInterface $builder, array $options) {
     
        $builder->add('busqueda', SearchType::class,[
            'label'=>false,
            'attr'=>[
                'class'=>'form-control mr-sm-2',
                'placeholder'=>'Busca título, país, estreno',
                'aria-label'=>'Search',
                ],
            ])
            ->add('submit', SubmitType::class,[
                'label'=>'Buscar',
                'attr'=>['class'=>'btn btn-outline-success my-2 my-sm-0'],
            ]);    
        
    }
    
    
}

