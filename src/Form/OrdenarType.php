<?php

namespace App\Form;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\Form\Extension\Core\Type\ChoiceType;
use Symfony\Component\Form\Extension\Core\Type\SubmitType;



class OrdenarType extends AbstractType{
    
    public function buildForm(FormBuilderInterface $builder, array $options) {
        
        $builder->add('ordena', ChoiceType::class,[
            'label'=>'Ordenar por:',
            'choices'=>[
                'Id'=>'idProduccion',
                'Titulo'=>'titulo',
                'Puntuación'=>'puntuacion',
                'Año estreno'=>'estreno',
                'Nacionalidad'=>'nacionalidad', 
            ],
        ])
        ->add('direccion', ChoiceType::class,[
            'label'=>false,
            'choices'=>[
                'Ascendente'=>'ASC',
                'Descendente'=>'DESC',
            ],
        ])        
        ->add('submit', SubmitType::class,[
            'label'=>'Ordena',
            
        ]);
        
    }
    
}

