<?php

namespace App\Form;

use App\Entity\Generos;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;
use Symfony\Bridge\Doctrine\Form\Type\EntityType;


/*Para hacer una colección de entityTypes y que te reconozca las opcionees seleccionadas, quitamos el BuildForm y
* metemos todas las opciones en el ConfigureOptions y hacemos la función getParent con el EntityType:
 */

class GenerosType extends AbstractType {

      public function configureOptions(OptionsResolver $resolver) {
        $resolver->setDefaults([
            'class'=> Generos::class,
            'data_class' => null,
            'choice_value'=>'idGenero',
            'choice_label'=>'genero',
            
        ]);
    }
    
    
     public function getParent()
    {
        return EntityType::class;
    }

}
