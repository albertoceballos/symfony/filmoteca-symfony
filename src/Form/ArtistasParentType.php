<?php

namespace App\Form;

use App\Entity\Artistas;

use Symfony\Bridge\Doctrine\Form\Type\EntityType;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\OptionsResolver\OptionsResolver;

class ArtistasParentType extends AbstractType
{
    
    public function configureOptions(OptionsResolver $resolver) {
        $resolver->setDefaults([
            'class'=> Artistas::class,
            'choice_label'=>'artista',
            'choice_value'=>'idArtista',
         ]);
    }
    
    public function getParent() {
        return EntityType::class;
    }
    
    
}

