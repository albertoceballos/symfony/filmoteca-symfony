<?php

namespace App\Form;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\Extension\Core\Type\IntegerType;
use Symfony\Component\Form\Extension\Core\Type\CollectionType;
use Symfony\Component\Form\Extension\Core\Type\SubmitType;
use Symfony\Component\Form\Extension\Core\Type\ChoiceType;
use App\Entity\Generos;

class ProduccionType extends AbstractType {

    public function buildForm(FormBuilderInterface $builder, array $options) {

        $builder->add('titulo', TextType::class, [
        'label' => 'Título',
        ])
        ->add('estreno', IntegerType::class, [
        'label' => 'Estreno',
        'data' => '2000',
        ])
        ->add('puntuacion', TextType::class, [
        'label' => 'Puntuación',
        'attr' => ['placeholder' => 'Debe ser un número'],
        ])
        ->add('vos', ChoiceType::class, [
        'label' => 'Version Original',
        'choices' => [
        'No' => 0,
        'Sí' => 1,
        ]
        ])
        ->add('nacionalidad', TextType::class, [
        'label' => 'Nacionaliad',
        ])
        ->add('vista', ChoiceType::class, [
        'label' => '¿Vista?',
        'choices' => [
        'No vista' => 0,
        'Vista' => 1,
        ]
        ]);
        $builder->add('idGenero', CollectionType::class, [
            'label'=>'Generos',
            'entry_type' => GenerosType::class,
            'entry_options' => ['label' => false],
            'allow_add' => true,
        ]);
        
        $builder->add('idArtista', CollectionType::class,[
            'label'=>'Artistas',
             'entry_options' => ['label' => false],
            'entry_type'=> ArtistasParentType::class,
            
        ]);

        $builder->add('submit', SubmitType::class, [
            'label' => 'Crear',
        ]);
    }

}
